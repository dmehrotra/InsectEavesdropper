import librosa
import os
import matplotlib.pyplot as plt
import concurrent.futures
from tqdm import tqdm 
from scipy import signal
import numpy as np
import pandas as pd
from csv import writer

os.chdir("/home/pc/DevD/Data")
a = os.listdir()
names = list()
# a.remove('.vscode')
for foldername in a:
    if os.path.isdir(foldername):
        names.append(list((map(lambda x: os.path.join(foldername, x), os.listdir(foldername)))))

names = names[0]+names[1]+names[2]+names[3]

exclusion_freqs = [60, 120, 180, 240, 300, 360, 420]
multiplier = 10
Q = 30.0
sample_rate = 22050
merge_seconds = 0.01
merge_frames = int(sample_rate * merge_seconds)
min_length = 0
peaks = pd.read_csv('/home/pc/DevD/Data/peakstype.csv')
downsampling_factor = 3
header = ['file_name','insect_name','type','Downsample','thres_signal','merged_intervals','snippets','length','max']

def merge_intervals(intervals, distance):
    merged = []
    current_start, current_stop = intervals[0]

    for start, stop in intervals[1:]:
        if start - current_stop <= distance:
            current_stop = stop
        else:
            merged.append((current_start, current_stop))
            current_start, current_stop = start, stop

    merged.append((current_start, current_stop))
    return merged

def simple_downsample(signal, factor,sr):
    # Use numpy.take to pick elements at regular intervals
    downsampled_signal = np.take(signal, range(0, len(signal), factor))
    return downsampled_signal,sr/factor
def extract(file_name):
    try:
    # Load audio file
        insect_name = file_name.split('/')[0]
        file_features_list = []

        try:
            type = peaks[peaks['file_name'] == file_name]['Type'].values[0]
        except:
            type = 'None'
        downsample = False
        out_signal, sample_rate = librosa.load(file_name,sr=44100)
        original = out_signal.copy()
        fs = sample_rate
        out_signal = out_signal.copy()

        # Apply notch filter for exclusion frequencies
        for f0 in exclusion_freqs:
            b, a = signal.iirnotch(f0, Q, fs)
            out_signal = signal.filtfilt(b, a, out_signal)

        # Find standard deviation of the signal
        thres_signal = np.std(out_signal)

        # Get absolute value of the signal
        abs_signal = np.abs(out_signal)

        # Extract subsignal based on the threshold
        subsignal_indices = np.where(abs_signal > thres_signal * multiplier)

        # Check if there are valid subsignals
        if len(subsignal_indices[0]) > 1:
            # Merge overlapping intervals
            merged_intervals = merge_intervals([(t, t) for t in subsignal_indices[0]], merge_frames)
            
            

            for start, stop in merged_intervals:
                length = (stop - start + 1) / sample_rate

                if length < min_length:
                    continue

                snippets, frames, maxa, lengthlist = [], [], 0, 0
                snip = out_signal[start - 100:stop + 200]

                snippets.append(snip)
                frames.append(length)
                maxa = (np.max(snip))
                lengthlist = (len(snip) - 300)
               
                file_features_list.extend([file_name, insect_name, type, downsample, thres_signal,(start, stop), str(snip), lengthlist, maxa])
                
           
        

    
        
            downsample = True
            out_signal, sample_rate = simple_downsample(original, downsampling_factor,sample_rate)
            fs = sample_rate
            out_signal = out_signal.copy()

            # Apply notch filter for exclusion frequencies
            for f0 in exclusion_freqs:
                b, a = signal.iirnotch(f0, Q, fs)
                out_signal = signal.filtfilt(b, a, out_signal)

            # Find standard deviation of the signal
            thres_signal = np.std(out_signal)

            # Get absolute value of the signal
            abs_signal = np.abs(out_signal)

            # Extract subsignal based on the threshold
            subsignal_indices = np.where(abs_signal > thres_signal * multiplier)

            # Check if there are valid subsignals
            if len(subsignal_indices[0]) > 1:
                # Merge overlapping intervals
                merged_intervals = merge_intervals([(t, t) for t in subsignal_indices[0]], merge_frames)
                
                

                for start, stop in merged_intervals:
                    length = (stop - start + 1) / sample_rate

                    if length < min_length:
                        continue

                    snippets, frames, maxa, lengthlist = [], [], 0, 0
                    snip = out_signal[start - 100:stop + 200]

                    snippets.append(snip)
                    frames.append(length)
                    maxa = (np.max(snip))
                    lengthlist = (len(snip) - 300)
                    file_features_list = []
                    file_features_list.extend([file_name, insect_name, type, downsample, thres_signal,(start, stop), str(snip), lengthlist, maxa])
                    
            else:
                
                return []
            
            if(os.path.isfile("downsample.csv")):
                                with open('downsample.csv', 'a') as f_object:
                                    writer_object = writer(f_object)
                                    writer_object.writerow(file_features_list)
                                    f_object.close()
            else:   
                            with open('downsample.csv', 'w') as f_object:
                                writer_object = writer(f_object)
                                writer_object.writerow(header)
                                writer_object.writerow(file_features_list)
                                f_object.close()
                    
                   
                            
                
        

        else:
            # No valid subsignals
            return []
    except:
        print(file_name)

def process_files(file_names):
    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = list(tqdm(executor.map(extract, file_names), total=len(file_names)))
        
        

def main():
    
    process_files(names)
    
if __name__ == '__main__':
        main()