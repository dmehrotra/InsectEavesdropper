# Project Name
Insect Eavesdropper

### Single Scripts

Contains scripts for data exploration.

### Multifile Scripts

Includes scripts for batch processing of audio files.

### Data

Consists of multiple CSV files:

- **Lab Features:**
  - Contains data related to laboratory experiments.

- **Arlington:**
  - Includes data from the pesticide trial conducted in Arlington.

- **West Madison:**
  - Encompasses phenotyping data.

## CSV Snippet Conversion to Numpy Array

To convert snippets in the CSV files to Numpy arrays, use the following code snippet:

```python
import numpy as np

# Applying operations to convert snippets to Numpy array
data['snippets'] = data['snippets'].apply(str.strip, args=('[]',)).apply(np.fromstring, dtype=float, sep=' ')


#Merge two csv file
import csv

def merge_csv(input_file1, input_file2, output_file):
    with open(input_file1, 'r') as infile1, open(input_file2, 'r') as infile2, open(output_file, 'w', newline='') as outfile:
        reader1 = csv.reader(infile1)
        reader2 = csv.reader(infile2)
        writer = csv.writer(outfile)

        # Write headers
        writer.writerow(next(reader1))
        writer.writerow(next(reader2))

        # Write data from the first file
        writer.writerows(reader1)

        # Skip the header in the second file
        next(reader2, None)

        # Write data from the second file
        writer.writerows(reader2)

# Example usage
input_csv1 = 'your_input_file_part1.csv'
input_csv2 = 'your_input_file_part2.csv'
output_csv = 'merged_output_file.csv'

merge_csv(input_csv1, input_csv2, output_csv)
